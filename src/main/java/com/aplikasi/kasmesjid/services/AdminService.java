package com.aplikasi.kasmesjid.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import com.aplikasi.kasmesjid.models.Admin;
import com.aplikasi.kasmesjid.repositories.AdminRepository;

@Service
public class AdminService {
	
	@Autowired
	private AdminRepository adminRepository;

	public List<Admin> getAdminWithFilter(Integer pageNo, Integer pageSize, String filter) {
		
		Pageable paging = PageRequest.of(pageNo, pageSize);
		Slice<Admin> pagedResult = adminRepository.getAllAdmin(filter, paging);
		List<Admin> listAdmin = pagedResult.getContent();
		
	return listAdmin;
	}
}
