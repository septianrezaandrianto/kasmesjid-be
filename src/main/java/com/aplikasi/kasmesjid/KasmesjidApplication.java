package com.aplikasi.kasmesjid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.aplikasi.kasmesjid.models.Admin;

@SpringBootApplication
@EnableJpaAuditing
@EnableConfigurationProperties(Admin.class)
public class KasmesjidApplication {

	public static void main(String[] args) {
		SpringApplication.run(KasmesjidApplication.class, args);
	}

}
