package com.aplikasi.kasmesjid.dtos;

import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AdminDTO {

	private long adminId;
	
	private String adminName;
	
	private String adminPhoneNumber;
	
	private String adminAddress;
	
	private String adminUsername;
	
	private String adminPassword;
	
	private String adminEmail;
	
	private String adminPhoto;
	
	private long createdBy;
	
	private Timestamp createdOn;
	
	private long lastModifiedBy;
	
	private Timestamp lastModifiedOn;
	
	private boolean isDeleted;
	
}
