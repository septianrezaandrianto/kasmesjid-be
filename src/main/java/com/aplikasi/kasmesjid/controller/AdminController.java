package com.aplikasi.kasmesjid.controller;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aplikasi.kasmesjid.dtos.AdminDTO;
import com.aplikasi.kasmesjid.models.Admin;
import com.aplikasi.kasmesjid.repositories.AdminRepository;
import com.aplikasi.kasmesjid.services.AdminService;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(origins = "http://localhost:8082")
@RestController
@RequestMapping("api")
public class AdminController {
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private AdminService adminService;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Code for converting entity admin to DTO
	private AdminDTO convertAdminToDTO (Admin admin) {
		AdminDTO adminDto = modelMapper.map(admin, AdminDTO.class);
	return adminDto;	
	}
	

//	Code for Getting all admin data
	@GetMapping("/admin/all")
	public HashMap<String, Object> getAllAdminData (@RequestParam(value = "pageNo") Integer pageNo,
			@RequestParam(value = "pageSize") Integer pageSize, @RequestParam(value = "filter") String filter){
		
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		ArrayList<HashMap<String, Object>> listMapAdmin = new ArrayList<HashMap<String, Object>>();
		ArrayList<AdminDTO> listAdminDto = new ArrayList<AdminDTO>();
		
		for (Admin admin: adminService.getAdminWithFilter(pageNo, pageSize, filter)) {			
			AdminDTO adminDto = convertAdminToDTO(admin);		
			
//			code for converting stamptime to "dd-MM-yyyy HH:mm:ss" format
			Date adminCreatedOn = new Date();
			Date adminLastModifiedOn = new Date();		
			adminCreatedOn.setTime(adminDto.getCreatedOn().getTime());
			String convertAdminCreatedOn = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(adminCreatedOn);
			adminLastModifiedOn.setTime(adminDto.getLastModifiedOn().getTime());
			String convertAdminLastModifiedOn = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(adminLastModifiedOn);
			
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("adminId", adminDto.getAdminId());
				mapTemp.put("adminName" , adminDto.getAdminName());
				mapTemp.put("adminPhoneNumber", adminDto.getAdminPhoneNumber());
				mapTemp.put("adminAddress", adminDto.getAdminAddress());
				mapTemp.put("adminuserName", adminDto.getAdminUsername());
				mapTemp.put("adminPassword", adminDto.getAdminPassword());
				mapTemp.put("adminEmail", adminDto.getAdminEmail());
				mapTemp.put("adminPhoto", adminDto.getAdminPhoto());
				mapTemp.put("createdBy", adminDto.getCreatedBy());
				mapTemp.put("createdOn", convertAdminCreatedOn);
				mapTemp.put("lastModifiedBy", adminDto.getLastModifiedBy());
				mapTemp.put("lastModifiedOn" , convertAdminLastModifiedOn);
				mapTemp.put("deleted", adminDto.isDeleted());
				
			listMapAdmin.add(mapTemp);
		}
		
//		Code for getting total data in database
		for (Admin admin : adminRepository.findAll()) {
			AdminDTO adminDto = convertAdminToDTO(admin);
			listAdminDto.add(adminDto);		
		}
		
		String message;
		if (listMapAdmin.isEmpty()) {
			message = "Admin data is empty";
		}
		else {
			message = "Show all admin data";
		}
		
		mapResult.put("Message" , message);
		mapResult.put("Total" , listAdminDto.size());
		mapResult.put("Data", listMapAdmin);
	return mapResult;
	}
	
	
//	Code for Getting admin data by id
	@GetMapping("/admin/adminId/{adminId}")
	public HashMap<String, Object> getAdminDataById (@PathVariable(value="adminId") long adminId) {
		
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
				
		Admin admin = adminRepository.findById(adminId).get();
		AdminDTO adminDto = convertAdminToDTO(admin);
		
//		Convert stamptime to "dd-MM-yyyy HH:mm:ss" format
		Date adminCreatedOn = new Date();
		Date adminLastModifiedOn = new Date();		
		adminCreatedOn.setTime(adminDto.getCreatedOn().getTime());
		String convertAdminCreatedOn = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(adminCreatedOn);
		adminLastModifiedOn.setTime(adminDto.getLastModifiedOn().getTime());
		String convertAdminLastModifiedOn = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(adminLastModifiedOn);		
		
			HashMap<String, Object> mapTemp = new HashMap<String, Object>();
			mapTemp.put("adminId", adminDto.getAdminId());
			mapTemp.put("adminName" , adminDto.getAdminName());
			mapTemp.put("adminPhoneNumber", adminDto.getAdminPhoneNumber());
			mapTemp.put("adminAddress", adminDto.getAdminAddress());
			mapTemp.put("adminuserName", adminDto.getAdminUsername());
			mapTemp.put("adminPassword", adminDto.getAdminPassword());
			mapTemp.put("adminEmail", adminDto.getAdminEmail());
			mapTemp.put("adminPhoto", adminDto.getAdminPhoto());
			mapTemp.put("createdBy", adminDto.getCreatedBy());
			mapTemp.put("createdOn", convertAdminCreatedOn);
			mapTemp.put("lastModifiedBy", adminDto.getLastModifiedBy());
			mapTemp.put("lastModifiedOn" , convertAdminLastModifiedOn);
			mapTemp.put("deleted", adminDto.isDeleted());
				
		mapResult.put("Message" , "Data admin with id " + admin.getAdminId());
		mapResult.put("Data", mapTemp);
	return mapResult;
	}
	
	
//	Code for creating / insert admin data to database
	@PostMapping("/admin/create")
	public HashMap<String, Object> createNewAdmin (@RequestParam(value="uploadFile") MultipartFile file, @RequestParam(value="textObject") String textObject) throws IOException {
		
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		
		ObjectMapper objectMapping = new ObjectMapper();
		Admin admin = objectMapping.readValue(textObject, Admin.class);
		
//		Save picture
		String directoryPhotos = "D:\\ecplise-workspace\\Reza-Project\\aplikasi-kasmesjid\\kasmesjid-be\\upload\\photos\\admin\\";
		String fileName = randomString() + file.getOriginalFilename();		
		file.transferTo(new File(directoryPhotos + fileName));		

		admin.setAdminPhoto(fileName);
		admin.setDeleted(false);
		adminRepository.save(admin);
		
		mapResult.put("Message" , "Success");
	return mapResult;
	}
	
//	Code for generate random string
	public String randomString() {
		String characters = "ABCDEFGHIJKLMNOPQRSTUabcdefghijklmnopqrstu1234567890";
		String randomResult = "";
		int randomLength = 25;
		
		Random random = new Random();
		
		char[] result = new char[randomLength];
		
		for (int i=0 ; i < randomLength; i++) {
			result[i] = characters.charAt(random.nextInt(characters.length()));
		}
		
		for (int j=0; j<result.length; j++) {
			randomResult += result[j];
		}
	return randomResult;
	}
	
	
//	Code for updating admin data
	@PutMapping("/admin/update/adminId/{adminId}")
	public HashMap<String, Object> updateAdminData (@PathVariable(value="adminId") long adminId, @RequestBody AdminDTO adminDto) {
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		
		Admin admin = adminRepository.findById(adminId).get();
		
		admin.setAdminAddress(adminDto.getAdminAddress());
		admin.setAdminEmail(adminDto.getAdminEmail());
		admin.setAdminName(adminDto.getAdminName());
		admin.setAdminPhoto(adminDto.getAdminPhoto());
		
		adminRepository.save(admin);
		
		mapResult.put("Message", "Update admin data success");
	return mapResult;
	}
	
	
//	Code for deleting admin data
	@DeleteMapping("/admin/delete/adminId/{adminId}")
	public HashMap<String, Object> deleteAdminData(@PathVariable(value="adminId") long adminId) {
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		
		Admin admin = adminRepository.findById(adminId).get();
		
		adminRepository.delete(admin);
		
		mapResult.put("Message", "Delete admin data success");
	return mapResult;
	}

} 
