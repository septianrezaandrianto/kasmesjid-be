package com.aplikasi.kasmesjid.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.aplikasi.kasmesjid.models.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Long> {

//	Query for pagination admin with filter name, username and email
	@Query(value="SELECT * FROM admin As a WHERE admin_name ~* :filter OR admin_username ~* :filter OR admin_email ~* :filter", nativeQuery = true)
	public Slice<Admin> getAllAdmin(@Param("filter") String filter, Pageable pageable);
}
