package com.aplikasi.kasmesjid.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="admin", schema="public")
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@ConfigurationProperties(prefix="file")
public class Admin implements Serializable {

	private static final long serialVersionUID =1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_admin_id_seq")
	@SequenceGenerator(name="generator_admin_id_seq", sequenceName = "admin_id_seq", schema = "public", allocationSize = 1)
	@Column(name="admin_id", unique = true, nullable = false)
	private long adminId;
	
	@Column(name="admin_name")
	private String adminName;
	
	@Column(name="admin_phone_number")
	private String adminPhoneNumber;
	
	@Column(name="admin_address")
	private String adminAddress;
	
	@Column(name="admin_username")
	private String adminUsername;
	
	@Column(name="admin_password")
	private String adminPassword;
	
	@Column(name="admin_email")
	private String adminEmail;
	
	@Column(name="admin_photo")
	private String adminPhoto;
	
	@Column(name="created_by")
	@CreatedBy
	private long createdBy;
	
	@Column(name="created_on", columnDefinition = "DATE")
	@CreatedDate
	private Timestamp createdOn;
	
	@Column(name="last_modified_by")
	@LastModifiedBy
	private long lastModifiedBy;
	
	@Column(name="last_modified_on", columnDefinition = "DATE")
	@LastModifiedDate
	private Timestamp lastModifiedOn;
	
	@Column(name="is_deleted")
	private boolean isDeleted;	
	
}
