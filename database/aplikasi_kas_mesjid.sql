/*
 Navicat Premium Data Transfer

 Source Server         : localhost_5432
 Source Server Type    : PostgreSQL
 Source Server Version : 120002
 Source Host           : localhost:5432
 Source Catalog        : aplikasi_kas_mesjid
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120002
 File Encoding         : 65001

 Date: 25/08/2020 16:33:04
*/


-- ----------------------------
-- Sequence structure for admin_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."admin_id_seq";
CREATE SEQUENCE "public"."admin_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 987654321
START 1
CACHE 1;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS "public"."admin";
CREATE TABLE "public"."admin" (
  "admin_id" int8 NOT NULL,
  "admin_name" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "admin_phone_number" varchar(20) COLLATE "pg_catalog"."default",
  "admin_address" text COLLATE "pg_catalog"."default" NOT NULL,
  "admin_username" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "admin_password" text COLLATE "pg_catalog"."default" NOT NULL,
  "admin_email" varchar(30) COLLATE "pg_catalog"."default",
  "admin_photo" text COLLATE "pg_catalog"."default",
  "created_by" int8,
  "created_on" timestamp(6),
  "last_modified_by" int8,
  "last_modified_on" timestamp(6),
  "is_deleted" bool
)
;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO "public"."admin" VALUES (23, 'Septian Reza', '0867958679876', 'Bekasi', 'admin', 'admin123', 'admin@gmail.com', 'fdLuJDIGhFULjAjCglKrQcJ1Creza.jpg', 0, '2020-07-18 14:12:21.061', 0, '2020-07-18 14:12:21.061', 'f');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."admin_id_seq"', 24, true);

-- ----------------------------
-- Primary Key structure for table admin
-- ----------------------------
ALTER TABLE "public"."admin" ADD CONSTRAINT "admin_pkey" PRIMARY KEY ("admin_id");
